# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka                                        | vyjádření                       |
| :-------------------------------------------- | :------------------------------ |
| jak dlouho mi tvorba zabrala - **čistý čas**  | cca 6 h                      |
| odkud jsem čerpal inspiraci                   | *spolužáci*                           |
| odkaz na video                                | https://youtu.be/rK04cOkFLys                     |
| jak se mi to podařilo rozplánovat             | rozplánovat dobře avšak zlomenina mi narušila plány                              |
| proč jsem zvolil tento design                 | kvůli ceně                              |
| zapojení                                      |   https://gitlab.spseplzen.cz/plachyr/moodlamp/-/blob/main/dokumentace/schema/mdlmp3.jpg      |
| z jakých součástí se zapojení skládá          | ESP8266 nano pro, WS2812, kabely, DHT22, 10KΩ R.                                |
| realizace                                     | https://gitlab.spseplzen.cz/plachyr/moodlamp/-/raw/main/dokumentace/fotky/mdlmp2.jpg |
| UI                                            |  https://gitlab.spseplzen.cz/plachyr/moodlamp/-/blob/main/dokumentace/fotky/uii.png     |
| co se mi povedlo                              | dokončit vše                              |
| co se mi nepovedlo/příště bych udělal/a jinak | nelámal si nohu, lépe pájel                               |
| zhodnocení celé tvorby (návrh známky)         | vše splněno, funguje, to je nejdůležitější známka 1-    |