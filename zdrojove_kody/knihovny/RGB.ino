#include <FastLED.h>

#define DATA_PIN    2
#define NUM_LEDS    60
#define BRIGHTNESS  200
#define LED_TYPE    WS2812B
#define COLOR_ORDER GRB

// Define the array of leds
CRGB leds[NUM_LEDS];

#define UPDATES_PER_SECOND 100

void setup() {
  FastLED.addLeds<LED_TYPE, DATA_PIN, COLOR_ORDER>(leds, NUM_LEDS);
  FastLED.setBrightness(BRIGHTNESS);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
}

void loop() {
  analogWrite(11, HIGH);
  ws2812_fill_all();
  moving_three_led();
  rgb_chasing();

}

void ws2812_fill_all() {
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB::Red;
    delay(100);
    FastLED.show();
  }
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB::Black;
    delay(100);
    FastLED.show();
  }
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB::Green;
    delay(100);
    FastLED.show();
  }
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB::Black;
    delay(100);
    FastLED.show();
  }
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB::Blue;
    delay(100);
    FastLED.show();
  }
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB::Black;
    delay(100);
    FastLED.show();
  }
}

void moving_three_led() {
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i]     = CRGB::Red;
    leds[i + 1] = CRGB::Green;
    leds[i + 2] = CRGB::Blue;
    FastLED.show();
    delay(100);
    for (int i = 0; i < NUM_LEDS; i++) {
      leds[i] = CRGB::Black;
      //delay(5);
    }
  }
}

void rgb_chasing() {
  int ms = 100;
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB(100, 0, 0);
    FastLED.show();
    delay(ms);
  }
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB(0, 100, 0);
    FastLED.show();
    delay(ms);
  }
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB(0, 0, 100);
    FastLED.show();
    delay(ms);
  }
}