#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define PIN D2

Adafruit_NeoPixel strip = Adafruit_NeoPixel(60, PIN, NEO_GRB + NEO_KHZ800);

const char* ssid = "3301-IoT";
const char* password = "mikrobus";
const char* mqtt_server = "10.202.31.211";

WiFiClient espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE	(50)
char msg[MSG_BUFFER_SIZE];
int value = 0;

// Allocate the JSON document
JsonDocument doc;
char buffer[400];

int red = D6, green = D5;

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  char RGB[200] = "";
  JsonDocument doc;
  DeserializationError error = deserializeJson(doc, payload, length);
  byte RGBs[3] = [doc["RGB"][0][0],doc["RGB"][0][1],doc["RGB"][0][2]];
  colorWipe(strip.Color(RGBs[0], RGBs[1], RGBs[2]), 50);

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      client.subscribe("skupina/4hs1/hofman/ESP/Node-RED");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}

void setup() {
  pinMode(BUILTIN_LED, OUTPUT);
  pinMode(D3, INPUT);
  pinMode(D4, OUTPUT);
   #if defined (__AVR_ATtiny85__)
    if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
  #endif
  strip.begin();
  strip.setBrightness(50);
  strip.show(); 
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

float getTemperature() {
  return random(200,220)/10.0;
}

void loop() {
  if (!client.connected()) {reconnect();}
  client.loop();
  unsigned long now = millis();
  if (now - lastMsg > 3000) {
    lastMsg = now;
   
    doc["dht"] = digitalRead(A3);

    doc["ip"] = WiFi.localIP();
    JsonArray RGB = doc["RGB"].to<JsonArray>();
      JsonArray RGB_0 = RGB.add<JsonArray>();
        RGB_0.add(200);
        RGB_0.add(200);
        RGB_0.add(200);
      JsonArray RGB_1 = RGB.add<JsonArray>();
        RGB_1.add(200);
        RGB_1.add(200);
        RGB_1.add(200);
      JsonArray RGB_2 = RGB.add<JsonArray>();
        RGB_2.add(200);
        RGB_2.add(200);
        RGB_2.add(200);
      JsonArray RGB_3 = RGB.add<JsonArray>();
        RGB_3.add(200);
        RGB_3.add(200);
        RGB_3.add(200);
      JsonArray RGB_4 = RGB.add<JsonArray>();
        RGB_4.add(200);
        RGB_4.add(200);
        RGB_4.add(200);
      JsonArray RGB_5 = RGB.add<JsonArray>();
        RGB_5.add(200);
        RGB_5.add(200);
        RGB_5.add(200);
      JsonArray RGB_6 = RGB.add<JsonArray>();
        RGB_6.add(200);
        RGB_6.add(200);
        RGB_6.add(200);
      JsonArray RGB_7 = RGB.add<JsonArray>();
        RGB_7.add(200);
        RGB_7.add(200);
        RGB_7.add(200);
    
    doc.shrinkToFit();  // optional
    serializeJson(doc, Serial);
    Serial.println();
    size_t n = serializeJson(doc, buffer);
    client.publish("skupina/4hs1/hofman/ESP/ESP_live", buffer, n);
  }
}
